import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as V from 'vis';
import { NetworkDataService } from '../networkService/network-data.service';
import { Store } from '@ngrx/store';
import { NetworkState, getCurrentAction } from './network.reducer';
import { Observable, BehaviorSubject } from 'rxjs';
import { CREATE_NODE_REQUEST } from '../actions/network.actions';
import { AppState } from '../reducers/reducer';

@Component({
    selector: 'network',
    templateUrl: './network.component.html',
    styleUrls: ['./network.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class NetworkComponent implements OnInit {
    network: V.Network;
    currentStyle: string;
    nodes: V.DataSet<V.Node>;
    edges: V.DataSet<V.Edge>;
    data: V.Data;
    currentAction : BehaviorSubject<string>

    constructor(private dataService: NetworkDataService, private store: Store<AppState>) {
        this.nodes = dataService.nodes;
        this.edges = dataService.edges;
    }

    ngOnInit(): void {
        var container = document.getElementById('mynetwork');

        this.data = {
            nodes: this.nodes,
            edges: this.edges
        };

        var options: V.Options = {
            nodes: {
                shape: 'dot',
                size: 30,
                font: {
                    size: 32,
                    color: '#ffffff'
                },
                borderWidth: 2
            },
            edges: {
                width: 2
            },
            manipulation: {
                addNode: (nodeData: V.NodeOptions, callback) => {
                    nodeData.label = 'hello world';
                    nodeData.shape = this.currentStyle;
                    callback(nodeData);
                }
            },
            interaction: { hover: true }
        };

        this.network = new V.Network(container, this.data, options);

        this.network.on("doubleClick", (params) => {
            if(params.nodes.length > 0){
                this.nodes.update({ id: params.nodes[0], label: 'TEST!' });
            }
            if(params.edges.length > 0){
                this.edges.update({id: params.edges[0], dashes: true});
            }
        });

        this.store.select(state => state.networkState.currentAction).subscribe((value) => {
            switch(value) {
                case CREATE_NODE_REQUEST: {
                      this.network.addNodeMode();
                }
                default: {
                }
            }
        });
    }

    handleStyleChanged($event) {
        this.currentStyle = $event;
    }
}

