import { createFeatureSelector, createSelector } from '@ngrx/store';
import *  as actions from '../actions/network.actions';
import * as V from 'vis';
import { AppState } from '../reducers/reducer';

export class NetworkState {
 nodes : V.Node[];
 edges : V.Node[];
 currentAction : string;
}

export const initialState: NetworkState = new NetworkState();

export function reducer(state = initialState, action: actions.AllActions): NetworkState {
  switch(action.type) {
    case actions.CREATE_NODE_REQUEST: {
      return {...state, currentAction: action.type};
    }
    default: {
      return state;
    }
  }	
}
export const getNetworkState = createFeatureSelector<AppState>('networkState');

export const getNodes = createSelector(
    getNetworkState,
    (state: AppState) => state.networkState.nodes
); 

export const getEdges = createSelector(
  getNetworkState,
  (state: AppState) => state.networkState.edges
); 

export const getCurrentAction= createSelector(
  getNetworkState,
  (state: AppState) => state.networkState.currentAction
); 