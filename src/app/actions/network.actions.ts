import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'

export const CREATE_NODE_REQUEST = 'CREATE_NODE_REQUEST';

export class CreateNodeClicked implements Action {
    readonly type = CREATE_NODE_REQUEST 

}
export class SomethingElse implements Action{
    readonly type = 'placeholder';

    constructor(public payload: string) {}
}

export type AllActions = CreateNodeClicked | SomethingElse;