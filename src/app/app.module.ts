import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NetworkComponent } from './network/network.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import {MatButtonModule, MatCheckboxModule, MatButtonToggleModule} from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers/reducer';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';
library.add(fas);

@NgModule({
   declarations: [
      AppComponent,
      NetworkComponent,
      NavigationComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      FontAwesomeModule,
      BrowserAnimationsModule,
      ToastrModule.forRoot(),
      MatButtonModule, 
      MatButtonToggleModule,
      MatCheckboxModule,
      StoreModule.forRoot(reducers, { metaReducers }),
      EffectsModule.forRoot([AppEffects])
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
