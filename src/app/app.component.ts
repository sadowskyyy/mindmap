import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NetworkComponent } from './network/network.component';
import { NavigationComponent } from './navigation/navigation.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  ngOnInit(): void {
 
  }

}