import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as networkReducer from '../network/network.reducer';

export interface AppState {
 networkState : networkReducer.NetworkState
}

export const reducers: ActionReducerMap<AppState> = 
{
  networkState: networkReducer.reducer
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
