import { Component, OnInit, Input, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import * as V from 'vis';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { NetworkState } from '../network/network.reducer';
import *  as actions from '../actions/network.actions';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit 
{
@Input() network: V.Network;
@Output() styleChangedEvent = new EventEmitter<string>();
currentStyle : string;

constructor(private toastr: ToastrService, private store: Store<NetworkState>) {}
  ngOnInit() {
  }

  changeStyle(style : string) : void{
    this.currentStyle = style;
    this.styleChangedEvent.emit(this.currentStyle);
  }

  createNode() : void {
    this.store.dispatch(new actions.CreateNodeClicked());
    //this.network.addNodeMode();
    this.toastr.success("Add your node!");
  }

  createLink() : void {
    this.network.addEdgeMode();
    this.toastr.info("Drag between nodes!");
  }

  deleteSelection() : void{
    this.network.deleteSelected();
    this.toastr.error("Deleted!");
  }

}
